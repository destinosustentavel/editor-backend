# Editor Backend

Backend do editor do Destino Sustentável.

## Ferramentas Utilizadas

* [Cors](https://www.npmjs.com/package/cors)
* [Express](https://expressjs.com/)
* [Node.js](https://nodejs.org/pt-br/)
* [Nodemon](https://www.npmjs.com/package/nodemon)

## Configuração do Ambiente

Para rodar este projeto no seu computador, é necessário que você tenha instalado o [Node.js](https://nodejs.org/en/download/) e um editor de código de sua preferência para facilitar a visualização dos arquivos. Para isso recomendamos o [Visual Studio Code](https://code.visualstudio.com/download) por ser de código aberto e fácil de utilizar.

## Como Utilizar

```bash
# clone este repositório
git clone https://gitlab.com/destinosustentavel/editor-backend.git

# instale as dependências
npm install

# execute a aplicação
npm run dev
```

## Como Contribuir

* Faça fork deste repositório
* Realize as mudanças no código
* Submeta um merge request
* Aguarde a avaliação da equipe de desenvolvimento

## Licença

[MIT License](https://opensource.org/licenses/MIT)
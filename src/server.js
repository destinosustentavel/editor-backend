const express = require("express");
const cors = require("cors");
const routes = require("./routes");

const app = express();

app.use(cors());
app.use(express.json());
app.use(routes);

// iniciando o servidor na porta 5000
app.listen(5000, () => {
  console.log("Servidor rodando na porta 5000");
});
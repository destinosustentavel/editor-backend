const fs = require("fs");

// definindo arquivo para leitura e gravação
const filename = "team.json";

module.exports = {
  // função para buscar dados
  async index(req, res) {
    fs.readFile(filename, "utf-8", (err, data) => {
      if (err || !data) return res.json({ error: "No data found" });

      const dataToJSON = JSON.parse(data.toString());

      return res.json(dataToJSON);
    });
  },

  // função para armazenar novos dados
  async store(req, res) {
    const teamMemberData = req.body;

    // se o arquivo não existir ele é criado
    fs.readFile(filename, "utf-8", (err, data) => {
      if (err && err.errno === -4058 || data.length === 0) {
        teamMemberData.id = 1;
        const dataToString = JSON.stringify([teamMemberData]);

        fs.writeFile(filename, dataToString, err => {
          if (err) return res.json({ error: err });
          
          return res.json({ message: "Team member successfully saved" });
        });
      } else if (data.length > 0) {
        const dataArr = JSON.parse(data.toString());
        teamMemberData.id = dataArr[dataArr.length - 1].id + 1;
        dataArr.push(teamMemberData);

        const dataToString = JSON.stringify(dataArr);
        fs.writeFile(filename, dataToString, err => {
          if (err) return res.json({ error: err });

          return res.json({ message: "Team member successfully saved" });
        });
      }
    });
  },

  // função para alteração dos dados
  async alter(req, res) {
    const { id, name, job, category, thumbnail } = req.body;

    fs.readFile(filename, "utf-8", (err, data) => {
      if (err || !data) return res.json({ error: "No data found" });

      const dataToJSON = JSON.parse(data.toString());
      const item = dataToJSON.find(item => item.id === id);
      
      item.name = name || item.name;
      item.job = job || item.job;
      item.category = category || item.category;
      item.thumbnail = thumbnail || item.thumbnail;

      const dataToString = JSON.stringify(dataToJSON);

      fs.writeFile(filename, dataToString, err => {
        if (err) return res.json({ error: err });

        return res.json({ message: "Team member successfully altered" });
      });
    });
  },

  // função para remoção dos dados
  async remove(req, res) {
    const { id } = req.params;

    fs.readFile(filename, "utf-8", (err, data) => {
      if (err || !data) return res.json({ error: "No data found" });

      const dataToJSON = JSON.parse(data.toString());
      const newDataArr = dataToJSON.filter(item => item.id !== Number(id));
      const dataToString = JSON.stringify(newDataArr);

      fs.writeFile(filename, dataToString, err => {
        if (err) return res.json({ error: err });

        return res.json({ message: "Team member successfully removed" });
      });
    });
  }
}
const fs = require("fs");
const bcrypt = require("bcrypt");

// definindo arquivo para leitura e gravação
const filename = "users.json";

module.exports = {
  // função para buscar dados
  async index(req, res) {
    const { username } = req.params;
    const { password } = req.body;

    fs.readFile(filename, "utf-8", (err, data) => {
      if (err || !data) return res.json({ error: "No data found" });

      const dataToJSON = JSON.parse(data.toString());
      const item = dataToJSON.find(item => item.username === username);

      if (typeof item === "undefined") return res.json({ authenticated: false });

      // verificando se a senha está correta
      bcrypt.compare(password, item.hash, (err, result) => {
        if (err) return res.json({ error: err });

        return res.json({ authenticated: result, hash: result ? item.hash : null });
      });
    });
  },

  // função para armazenar novos dados
  async store(req, res) {
    const { username, password } = req.body;

    // gerando salt para diferenciar a senha
    const salt = await bcrypt.genSalt();

    // gerando hash da senha
    const hash = await bcrypt.hash(password, salt);

    // se o arquivo não existir ele é criado
    fs.readFile(filename, "utf-8", (err, data) => {
      if (err && err.errno === -4058 || data.length === 0) {
        const dataToString = JSON.stringify([{ username, hash }]);

        fs.writeFile(filename, dataToString, err => {
          if (err) return res.json({ error: err });

          return res.json({ message: "User successfully saved" });
        });
      } else if (data.length > 0) {
        const dataArr = JSON.parse(data.toString());
        dataArr.push({ username, hash });

        const dataToString = JSON.stringify(dataArr);
        fs.writeFile(filename, dataToString, err => {
          if (err) return res.json({ error: err });

          return res.json({ message: "User successfully saved" });
        });
      }
    });
  }
}
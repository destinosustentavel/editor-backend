const express = require("express");

const UserController = require("./controllers/UserController");
const TeamController = require("./controllers/TeamController");
const AwardsController = require("./controllers/AwardsController");

const routes = express.Router();

// rotas dos usuários
routes.post("/user/:username", UserController.index);
routes.post("/user", UserController.store);

// rotas da equipe
routes.get("/team", TeamController.index);
routes.post("/team", TeamController.store);
routes.put("/team", TeamController.alter);
routes.delete("/team/:id", TeamController.remove);

// rotas dos prêmios e títulos
routes.get("/awards", AwardsController.index);
routes.post("/awards", AwardsController.store);
routes.put("/awards", AwardsController.alter);
routes.delete("/awards/:id", AwardsController.remove);

module.exports = routes;